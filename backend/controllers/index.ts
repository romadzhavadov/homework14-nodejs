import NewsController from "./news.controller";
import UsersController from "./users.controller";


export {
    NewsController,
    UsersController
}