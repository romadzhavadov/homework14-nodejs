import { faker } from '@faker-js/faker';
import AppDataSource from '../dbtypeORM/data-source';
import { User } from '../dbtypeORM/entity/User';
import { Newspost } from '../dbtypeORM/entity/Newpost';

async function seed() {
  try {
    await AppDataSource.initialize();
    console.log("Data Source has been initialized!");

    // Створення користувача
    const user = new User();
    user.password = faker.internet.password();
    user.email = faker.internet.email();
    await AppDataSource.manager.save(user);

    // Створення 20 новин та пов'язання їх із створеним користувачем
    for (let i = 0; i < 20; i++) {
      const newspost = new Newspost();
      newspost.title = faker.lorem.sentence();
      newspost.text = faker.lorem.paragraph();
      newspost.author = user;
      await AppDataSource.manager.save(newspost);
    }
  } catch (error) {
    console.error("Error during Data Source initialization", error);
  } finally {
    await AppDataSource.close();
  }
}

seed().catch(console.error);