import { Router } from 'express';
import NewsRouter from './news.routes';
import UsersRouter from './users.routes';
import LoginRouter from './login.routes';

interface RoutesInterface {
    [index: string]: Router
  }

export const ApiRoutes: RoutesInterface = {
  newsposts: NewsRouter,
}

export const AuthRoutes: RoutesInterface = {
  register: UsersRouter,
  login: LoginRouter,
  user: UsersRouter,
}

// export default Routes;