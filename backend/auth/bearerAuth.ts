import passport from 'passport';
import { Strategy } from 'passport-http-bearer';
import UserRepositoryOrm from '../dbtypeORM/userRepositoryOrm';
import hashPassword from './hashPassword';


const authMiddleware = async (
    token: string,
    done: (err: Error | null, user?: any) => void
) => {
    console.log("token", token)
    const unhashedToken = Buffer.from(token, 'base64').toString('utf-8')
    const [ email,  password ] = unhashedToken.split(':')
    console.log("unhashedToken", unhashedToken)

    let err: any = null
    const user = await UserRepositoryOrm.getUser(email);

    console.log("find", err, user)

    if (err) {
        console.log("DONE ERR", err)
        return done(err); 
    }
    if (!user) { 
        console.log("DONE NO USER")
        return done(null, null); 
    }

    console.log("password", password, user.password, hashPassword(password))
    if (user.password !== hashPassword(password)) {
        console.log("DONE !PASSWORD")
        return done(null, null); 
    }

    console.log("DONE SUCCESS", user)

    done(null, user);
}

const bearerStrategy = new Strategy(authMiddleware)

passport.use(bearerStrategy);

export default passport.authenticate('bearer', { session: false });

