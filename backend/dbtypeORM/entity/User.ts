import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { Newspost } from './Newpost';

@Entity({ name: "users" })
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @Column()
  password: string;

  @OneToMany(() => Newspost, (newspost) => newspost.author)
  newsposts: Newspost[];
}

