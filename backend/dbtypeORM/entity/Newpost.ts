import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { User } from './User'

enum Genre {
  POLITIC = 'Politic',
  BUSINESS = 'Business',
  SPORT = 'Sport',
  OTHER = 'Other'
}

@Entity()
export class Newspost {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  text: string;

  @Column({
    type: 'enum',
    enum: Genre,
    default: Genre.OTHER
  })
  genre: Genre;

  @ManyToOne(() => User, (user) => user.newsposts)
  author: User;
}



