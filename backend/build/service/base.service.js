var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import UserRepositoryOrm from '../dbtypeORM/userRepositoryOrm';
import NewspostRepositoryOrm from '../dbtypeORM/newspostRepositoryOrm';
import hashPassword from '../auth/hashPassword';
import createToken from '../auth/createToken';
export class BaseService {
    constructor() {
        this.addNews = this.addNews.bind(this);
        this.getItems = this.getItems.bind(this);
        this.getOneItems = this.getOneItems.bind(this);
        this.updateNews = this.updateNews.bind(this);
        this.deleteNews = this.deleteNews.bind(this);
        this.registerUser = this.registerUser.bind(this);
        this.loginUser = this.loginUser.bind(this);
    }
    getItems(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const page = Number(req.query.page) || 1;
            const size = Number(req.query.size) || 4;
            const data = yield NewspostRepositoryOrm.readAll({
                page,
                size,
            });
            return data;
        });
    }
    getOneItems(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield NewspostRepositoryOrm.read(Number(req.params.id));
            return data;
        });
    }
    addNews(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield NewspostRepositoryOrm.create(req);
            return data;
        });
    }
    updateNews(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield NewspostRepositoryOrm.read(Number(req.params.id));
            if (!data) {
                return { errorMessage: "News not found" };
            }
            const updated = yield NewspostRepositoryOrm.update(Number(req.params.id), req.body);
            return updated;
        });
    }
    deleteNews(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield NewspostRepositoryOrm.read(Number(req.params.id));
            if (!data) {
                return { errorMessage: "News not found" };
            }
            yield NewspostRepositoryOrm.delete(Number(req.params.id));
            return { message: `News was  removed` };
        });
    }
    registerUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            // Валідація даних користувача
            const { email, password, confirmPassword } = req.body;
            // Перевірка, чи співпадають паролі
            if (password !== confirmPassword) {
                return { errorMessage: "Passwords do not match" };
            }
            if (email) {
                const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
                if (!emailRegex.test(email)) {
                    return { errorMessage: "Invalid email format" };
                }
            }
            const hashedPassword = hashPassword(password);
            const data = {
                email: email,
                password: hashedPassword
            };
            const user = yield UserRepositoryOrm.registerUser(data);
            if (user) {
                return createToken(email, password);
            }
        });
    }
    loginUser(table, req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { email, password } = req.body;
            const user = yield UserRepositoryOrm.getUser(email);
            if (!user) {
                throw new Error(`Item with email ${email} not found.`);
            }
            if (user.password === hashPassword(password)) {
                return createToken(email, password);
            }
        });
    }
    // static async getUserByEmail(email: string) {
    //     const user = await UserRepositoryOrm.getUser( email);
    //     return user
    // }
    getAuthUser(table, req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { email, password } = req.body;
            const user = yield UserRepositoryOrm.getUser(email);
            return user;
        });
    }
}
//# sourceMappingURL=base.service.js.map