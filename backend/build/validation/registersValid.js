import Validator from 'fastest-validator';
export const validatorService = new Validator();
export const registersSchema = {
    email: { type: "email", max: 50 },
    password: { type: "string", max: 50 },
};
export const validateRegisters = validatorService.compile(registersSchema);
//# sourceMappingURL=registersValid.js.map