var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import axios from 'axios';
const main = () => __awaiter(void 0, void 0, void 0, function* () {
    const URL = 'http://localhost:8000';
    // const response = await axios.get(URL);
    // console.log("main", response.data);
    const createUser = yield axios
        .post(`${URL}/auth/register/`, { email: "test2@test.com", password: "00000000", confirmPassword: "00000000" })
        .catch((e) => {
        console.log("e", e.response.data);
        return e.response;
    });
    console.log("createUser", createUser.data);
    const signIn = yield axios.post(`${URL}/auth/login/`, { email: "test2@test.com", password: "00000000" });
    console.log("signIn", signIn.data);
    // const getUser = await axios.get(`${URL}/user/${createUser.data.data.id}`);
    // console.log("getUser", getUser.data);
    // const updateUser = await axios.put(`${URL}/user/${createUser.data.data.id}`, { name: "ObjectMan", email: "test2@test.com", phone: "+9999" });
    // console.log("updateUser", updateUser.data);
    // const deleteUser = await axios.delete(`${URL}/user/${createUser.data.data.id}`);
    // console.log("deleteUser", deleteUser.data);
    // const createPost = await axios.post(`${URL}/post`, {
    //     title: "Hello",
    //     content: "World"
    // });
    console.log("getPosts", signIn.data);
    const getPosts = yield axios.get(`${URL}/api/newsposts/`, {
        headers: {
            Authorization: `Bearer ${signIn.data.token}`
        }
    });
    console.log("getPosts", getPosts.data);
    // const post = await axios.post(`${URL}/post`, {
    //     title: "Hello",
    //     content: "Hello people this is first " + Math.floor(Math.random() * 1000)
    // }, {
    //     headers: {
    //         Authorization: `Bearer ${signIn.data.token}`
    //     }
    // });
    // console.log("post", post.data);
    // await axios.post(`${URL}/post`, {
    //     title: "Hello",
    //     content: "Hello people this is second " + Math.floor(Math.random() * 1000)
    // });
    // await axios.post(`${URL}/post`, {
    //     title: "Hello",
    //     content: "Hello people this is three " + Math.floor(Math.random() * 1000)
    // });
    // console.log("createPost", createPost.data);
    // const getPost = await axios.get(`${URL}/post/${createPost.data.data.id}`);
    // console.log("getPost", getPost.data);
    // const updatePost = await axios.put(`${URL}/post/${createPost.data.data.id}`, {
    //     title: "Hello",
    //     content: "World2"
    // });
    // console.log("updatePost", updatePost.data);
    // const deletePost = await axios.delete(`${URL}/post/${createPost.data.data.id}`);
    // console.log("deletePost", deletePost.data);
    // // get all posts
    // const getPosts = await axios.get(`${URL}/post`);
    // console.log("getPosts", getPosts.data.data);
    // console.table(getPosts.data.data.items);
});
main().catch(e => console.log("error", e.config.url, e.config.method, e.response.data));
//# sourceMappingURL=test.js.map