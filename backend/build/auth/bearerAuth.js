var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import passport from 'passport';
import { Strategy } from 'passport-http-bearer';
import UserRepositoryOrm from '../dbtypeORM/userRepositoryOrm';
import hashPassword from './hashPassword';
const authMiddleware = (token, done) => __awaiter(void 0, void 0, void 0, function* () {
    console.log("token", token);
    const unhashedToken = Buffer.from(token, 'base64').toString('utf-8');
    const [email, password] = unhashedToken.split(':');
    console.log("unhashedToken", unhashedToken);
    let err = null;
    const user = yield UserRepositoryOrm.getUser(email);
    console.log("find", err, user);
    if (err) {
        console.log("DONE ERR", err);
        return done(err);
    }
    if (!user) {
        console.log("DONE NO USER");
        return done(null, null);
    }
    console.log("password", password, user.password, hashPassword(password));
    if (user.password !== hashPassword(password)) {
        console.log("DONE !PASSWORD");
        return done(null, null);
    }
    console.log("DONE SUCCESS", user);
    done(null, user);
});
const bearerStrategy = new Strategy(authMiddleware);
passport.use(bearerStrategy);
export default passport.authenticate('bearer', { session: false });
//# sourceMappingURL=bearerAuth.js.map