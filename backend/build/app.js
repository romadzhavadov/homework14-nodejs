var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import express from 'express';
import cors from 'cors';
import { AuthRoutes, ApiRoutes } from './routes';
import dotenv from 'dotenv';
import { logRequests, errorLogger, logger } from './logers/logger';
import "reflect-metadata";
import AppDataSource from './dbtypeORM/dataSource';
const PORT = process.env.PORT || 8000;
class App {
    constructor() {
        this.app = express();
        // this.configurrDbConnection()
    }
    configureDbConnection() {
        if (!AppDataSource.isInitialized) {
            AppDataSource.initialize()
                .then(() => {
                console.log("Data Source has been initialized!");
            })
                .catch((err) => {
                console.error("Error during Data Source initialization", err);
            });
        }
    }
    routing() {
        this.app.get('/', (req, res) => {
            res.sendFile('index.html', { root: './static' });
        });
        const initRoute = (route, Routes) => {
            Object.keys(Routes).forEach((key) => {
                if (route === "user") {
                    this.app.use(`/${route}`, Routes[key]);
                }
                this.app.use(`/${route}/${key}`, Routes[key]);
                //   this.app.use(`/api/${key}`, Routes[key]);
            });
        };
        initRoute("auth", AuthRoutes);
        initRoute("api", ApiRoutes);
        initRoute("user", AuthRoutes);
    }
    initPlugins() {
        this.app.use(express.json());
        this.app.use(express.static('static'));
        this.app.use(cors());
        this.app.use(logRequests);
        this.app.use(errorLogger);
    }
    start() {
        return __awaiter(this, void 0, void 0, function* () {
            // if (process.env.NODE_ENV !== 'production') {
            //   await DatabaseService.createTables();
            // }
            this.configureDbConnection();
            this.initPlugins();
            this.routing();
            this.app.listen(PORT, () => {
                logger.info(`Server is running on port ${PORT}`);
            });
        });
    }
}
dotenv.config();
const app = new App();
app.start();
//# sourceMappingURL=app.js.map