var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { faker } from '@faker-js/faker';
import { AppDataSource } from '../dbtypeORM/dataSource.js';
import { User } from '../dbtypeORM/entity/User';
import { Newspost } from '../dbtypeORM/entity/Newpost';
function seed() {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield AppDataSource.initialize();
            console.log("Data Source has been initialized!");
            // Створення користувача
            const user = new User();
            user.password = faker.internet.password();
            user.email = faker.internet.email();
            yield AppDataSource.manager.save(user);
            // Створення 20 новин та пов'язання їх із створеним користувачем
            for (let i = 0; i < 20; i++) {
                const newspost = new Newspost();
                newspost.title = faker.lorem.sentence();
                newspost.text = faker.lorem.paragraph();
                newspost.author = user;
                yield AppDataSource.manager.save(newspost);
            }
        }
        catch (error) {
            console.error("Error during Data Source initialization", error);
        }
        finally {
            yield AppDataSource.close();
        }
    });
}
seed().catch(console.error);
//# sourceMappingURL=seed.js.map