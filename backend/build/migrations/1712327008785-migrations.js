var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { TableColumn } from "typeorm";
export class Migrations1712327008785 {
    up(queryRunner) {
        return __awaiter(this, void 0, void 0, function* () {
            // Додаємо поле "deleted" до таблиці "newpost" та "users"
            yield queryRunner.addColumn("newpost", new TableColumn({
                name: "deleted",
                type: "boolean",
                default: false
            }));
            yield queryRunner.addColumn("users", new TableColumn({
                name: "deleted",
                type: "boolean",
                default: false
            }));
        });
    }
    down(queryRunner) {
        return __awaiter(this, void 0, void 0, function* () {
            yield queryRunner.dropColumn("newpost", "deleted");
            yield queryRunner.dropColumn("users", "deleted");
        });
    }
}
//# sourceMappingURL=1712327008785-migrations.js.map