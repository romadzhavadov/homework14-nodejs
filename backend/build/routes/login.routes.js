import { Router } from 'express';
import { UsersController } from '../controllers';
class LoginRouter {
    constructor() {
        this.router = Router();
        this.controller = new UsersController();
        this.routes();
    }
    routes() {
        this.router.route('/error')
            .get(this.controller.getError);
        this.router.route('/')
            .post(this.controller.loginUser);
    }
}
const { router } = new LoginRouter();
export default router;
//# sourceMappingURL=login.routes.js.map