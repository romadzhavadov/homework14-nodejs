import { Router } from 'express';
import { UsersController } from '../controllers';
class UsersRouter {
    constructor() {
        this.router = Router();
        this.controller = new UsersController();
        this.routes();
    }
    routes() {
        this.router.route('/error')
            .get(this.controller.getError);
        this.router.route('/')
            .post(this.controller.registerUser);
        this.router.route('/')
            .get(this.controller.getAuthUser);
    }
}
const { router } = new UsersRouter();
export default router;
//# sourceMappingURL=users.routes.js.map