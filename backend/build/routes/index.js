import NewsRouter from './news.routes';
import UsersRouter from './users.routes';
import LoginRouter from './login.routes';
export const ApiRoutes = {
    newsposts: NewsRouter,
};
export const AuthRoutes = {
    register: UsersRouter,
    login: LoginRouter,
    user: UsersRouter,
};
// export default Routes;
//# sourceMappingURL=index.js.map