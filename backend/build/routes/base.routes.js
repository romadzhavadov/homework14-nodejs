import { Router } from 'express';
import passportBearerMiddleware from '../auth/bearerAuth';
class BaseRouter {
    constructor(controller) {
        this.router = Router();
        this.controller = controller;
        this.routes();
    }
    routes() {
        this.router.route('/')
            .post(passportBearerMiddleware, this.controller.addNews)
            .get(passportBearerMiddleware, this.controller.getItems);
        this.router.route('/error')
            .get(this.controller.getError);
        this.router.route('/:id')
            .get(passportBearerMiddleware, this.controller.getOneItems)
            .put(passportBearerMiddleware, this.controller.updateNews)
            .delete(passportBearerMiddleware, this.controller.deleteNews);
    }
}
export default BaseRouter;
//# sourceMappingURL=base.routes.js.map