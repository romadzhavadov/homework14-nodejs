var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { User } from "./entity/User";
import AppDataSource from "./dataSource";
// const userRepositiry = AppDataSource.getRepository(User)
// клас DatabaseService це DAL (Data Access Layer) для роботи з базою даних
class UserRepositoryOrm {
    constructor() {
        this.db = AppDataSource.getRepository(User);
    }
    // ініціалізація екземпляра класу(патерн Singleton)
    static getInstance() {
        if (!UserRepositoryOrm.instance) {
            UserRepositoryOrm.instance = new UserRepositoryOrm();
        }
        return UserRepositoryOrm.instance;
    }
    // метод запису нового  юзера
    registerUser(data) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const user = new User();
                user.email = data.email;
                user.password = data.password;
                const newUserSaved = yield this.db.manager.save(user);
                return newUserSaved;
            }
            catch (error) {
                console.error('Error creating new user:', error);
                throw error;
            }
        });
    }
    getUser(email) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const getOneUser = yield this.db.findOne({ where: { email } });
                return getOneUser;
            }
            catch (error) {
                console.error("Error reading item:", error);
                throw error;
            }
        });
    }
}
// експортування екземпляра класу
export default UserRepositoryOrm.getInstance();
//# sourceMappingURL=userRepositoryOrm.js.map