import "reflect-metadata";
import { DataSource } from "typeorm";
import { User } from "./entity/User";
import { Newspost } from "./entity/Newpost";
// import { mig } from "../migrations/1712327008785-migrations" 
const AppDataSource = new DataSource({
    type: "mysql",
    host: "localhost",
    port: 3306,
    username: "root",
    password: "12345qazwsxed",
    database: "databaseorm",
    synchronize: true,
    logging: false,
    entities: [User, Newspost],
    migrations: ["migrations/*.ts"],
    subscribers: [],
});
export default AppDataSource;
//# sourceMappingURL=dataSource.js.map