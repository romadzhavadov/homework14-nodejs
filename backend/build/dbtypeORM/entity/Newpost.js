var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { User } from './User';
var Genre;
(function (Genre) {
    Genre["POLITIC"] = "Politic";
    Genre["BUSINESS"] = "Business";
    Genre["SPORT"] = "Sport";
    Genre["OTHER"] = "Other";
})(Genre || (Genre = {}));
let Newspost = class Newspost {
};
__decorate([
    PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], Newspost.prototype, "id", void 0);
__decorate([
    Column(),
    __metadata("design:type", String)
], Newspost.prototype, "title", void 0);
__decorate([
    Column(),
    __metadata("design:type", String)
], Newspost.prototype, "text", void 0);
__decorate([
    Column({
        type: 'enum',
        enum: Genre,
        default: Genre.OTHER
    }),
    __metadata("design:type", String)
], Newspost.prototype, "genre", void 0);
__decorate([
    ManyToOne(() => User, (user) => user.newsposts),
    __metadata("design:type", User)
], Newspost.prototype, "author", void 0);
Newspost = __decorate([
    Entity()
], Newspost);
export { Newspost };
//# sourceMappingURL=Newpost.js.map