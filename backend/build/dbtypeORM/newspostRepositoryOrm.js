var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import AppDataSource from "./dataSource";
import { Newspost } from "./entity/Newpost";
// const userRepositiry = AppDataSource.getRepository(User)
// клас DatabaseService це DAL (Data Access Layer) для роботи з базою даних
class NewspostRepositoryOrm {
    constructor() {
        this.db = AppDataSource.getRepository(Newspost);
    }
    // ініціалізація екземпляра класу(патерн Singleton)
    static getInstance() {
        if (!NewspostRepositoryOrm.instance) {
            NewspostRepositoryOrm.instance = new NewspostRepositoryOrm();
        }
        return NewspostRepositoryOrm.instance;
    }
    // // метод для створення нового запису у базі даних для вказаної таблиці
    create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const newspost = new Newspost();
                newspost.title = data.body.title;
                newspost.text = data.body.text;
                newspost.author = data.user.id;
                const newPostSaved = yield this.db.manager.save(newspost);
                console.log('New newspost saved:', newPostSaved);
                return newPostSaved;
            }
            catch (error) {
                console.error('Error creating new newspost:', error);
                throw error;
            }
        });
    }
    readAll(params) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const offset = (params.page - 1) * params.size;
                // Зчитування елементів з бази даних
                const items = yield this.db.find({
                    relations: ["author"],
                    take: params.size,
                    skip: offset,
                });
                // Зчитування загальної кількості елементів
                const count = yield this.db.manager.count(Newspost);
                console.log("Всі НОВИНИ", items);
                return {
                    items,
                    count,
                };
            }
            catch (error) {
                console.error("Error reading data:", error);
                throw error;
            }
        });
    }
    // метод для отримання одного запису з бази даних
    read(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const getOneItem = yield this.db.findOne({ relations: ["author"], where: { id } });
                return getOneItem;
            }
            catch (error) {
                console.error("Error reading item:", error);
                throw error;
            }
        });
    }
    // метод для оновлення одного запису
    update(id, newData) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const itemToUpdate = yield this.db.findOne({ where: { id } });
                const updatedItem = yield this.db.save(Object.assign(Object.assign({}, itemToUpdate), newData));
                return updatedItem;
            }
            catch (error) {
                console.error("Error updating item:", error);
                throw error;
            }
        });
    }
    // // метод для видалення одного запису
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const itemToDelete = yield this.db.findOne({ where: { id } });
                yield this.db.delete(itemToDelete);
                return { message: `Item with id ${id} deleted successfully` };
            }
            catch (error) {
                console.error("Error updating item:", error);
                throw error;
            }
        });
    }
}
// експортування екземпляра класу
export default NewspostRepositoryOrm.getInstance();
//# sourceMappingURL=newspostRepositoryOrm.js.map