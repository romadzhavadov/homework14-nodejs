var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { validateNewspost } from '../validation/valid';
import { validateRegisters } from '../validation/registersValid';
import { ValidationError, NewsRepositoryError } from '../errors/Errors';
export class BaseController {
    constructor(table, service) {
        this.table = table;
        this.service = service;
        this.addNews = this.addNews.bind(this);
        this.getItems = this.getItems.bind(this);
        this.getOneItems = this.getOneItems.bind(this);
        this.updateNews = this.updateNews.bind(this);
        this.deleteNews = this.deleteNews.bind(this);
        this.registerUser = this.registerUser.bind(this);
        this.loginUser = this.loginUser.bind(this);
    }
    getItems(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const data = yield this.service.getItems(req, res);
                res.status(200).json({ data });
                if (data === undefined) {
                    res.status(200).json([]);
                }
            }
            catch (error) {
                console.error("Error find news:", error);
                res.status(500).json({ error: "Error find news" });
            }
        });
    }
    getOneItems(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const data = yield this.service.getOneItems(req, res);
                if (data) {
                    res.status(200).json({ data, message: `${this.table} read` });
                }
                else {
                    res.status(404).json({ errorMessage: "News not found" });
                }
            }
            catch (error) {
                console.error("Error find news:", error);
                res.status(500).json({ error: "Error find news" });
            }
        });
    }
    addNews(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                console.log('ADDD NEWSSSSSSSSSS', req.body);
                console.log('ADDD USERRRRRRR', req.user);
                const validate = validateNewspost(req.body);
                if (validate !== true) {
                    throw new ValidationError('Validation error', validate);
                }
                const data = yield this.service.addNews(req, res);
                res.status(201).json({ message: `${this.table} created`, data });
            }
            catch (error) {
                if (error instanceof ValidationError) {
                    console.error("Validation error:", error);
                    res.status(400).json({ error: error.message, details: error.details });
                }
                else {
                    console.error("Error updating news:", error);
                    res.status(500).json({ error: "Error adding news" });
                }
            }
        });
    }
    updateNews(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const validate = validateNewspost(req.body);
                if (validate !== true) {
                    throw new ValidationError('Validation error', validate);
                }
                const data = yield this.service.updateNews(req, res);
                if (data.errorMessage) {
                    res.status(404).json(data.errorMessage);
                }
                res.status(200).json({ data, message: `${this.table} updated` });
            }
            catch (error) {
                if (error instanceof ValidationError) {
                    console.error("Validation error:", error);
                    res.status(400).json({ error: error.message, details: error.details });
                }
                else {
                    console.error("Error updating news:", error);
                    res.status(500).json({ error: "Error updating news" });
                }
            }
        });
    }
    deleteNews(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const data = yield this.service.deleteNews(req, res);
                if (data.errorMessage) {
                    res.status(404).json(data.errorMessage);
                }
                res.status(200).json(data);
            }
            catch (error) {
                console.error("Error deleting news:", error);
                res.status(500).json({ error: "Error deleting news" });
            }
        });
    }
    getError(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                throw new NewsRepositoryError('Error in NewsRepository');
            }
            catch (error) {
                if (error instanceof NewsRepositoryError) {
                    console.error("NewsRepository error:", error);
                    res.status(500).json({ error: error.message });
                }
                else {
                    console.error("Unknown error:", error);
                    res.status(500).json({ error: "Unknown error" });
                }
            }
        });
    }
    registerUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const token = yield this.service.registerUser(req, res);
                const validate = validateRegisters(req.body);
                if (validate !== true) {
                    throw new ValidationError('Validation error', validate);
                }
                // Повернення токена відповідь на клієнт
                res.status(201).json({ token: `Bearer ${token}`, massage: "Successful registration" });
            }
            catch (error) {
                if (error instanceof ValidationError) {
                    console.error("Validation error:", error);
                    res.status(400).json({ error: error.message, details: error.details });
                }
                else {
                    console.error("Error registering user:", error);
                    res.status(500).json({ error: "Error registering user" });
                }
            }
        });
    }
    loginUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                console.log(req.body);
                // passport.authenticate('bearer', { session: false });
                const validate = validateRegisters(req.body);
                if (validate !== true) {
                    throw new ValidationError('Validation error', validate);
                }
                const token = yield this.service.loginUser(this.table, req, res);
                if (token) {
                    res.status(201).json({ token: `Bearer ${token}`, massage: "Authentication succses" });
                }
                else {
                    // Обробка помилки аутентифікації, якщо токен не отримано
                    console.error("Authentication error: Token not received");
                    res.status(401).json({ error: "Authentication error: Token not received" });
                }
            }
            catch (error) {
                if (error instanceof ValidationError) {
                    console.error("Validation error:", error);
                    res.status(400).json({ error: error.message, details: error.details });
                }
                else {
                    console.error("Error registering user:", error);
                    res.status(500).json({ error: "Error registering user" });
                }
            }
        });
    }
    getAuthUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const user = yield this.service.getAuthUser(this.table, req, res);
                res.status(201).json({ user, massage: "Authentication succses" });
            }
            catch (error) {
                if (error instanceof ValidationError) {
                    console.error("Validation error:", error);
                    res.status(400).json({ error: error.message, details: error.details });
                }
                else {
                    console.error("Error registering user:", error);
                    res.status(500).json({ error: "Error registering user" });
                }
            }
        });
    }
}
//# sourceMappingURL=base.controller.js.map