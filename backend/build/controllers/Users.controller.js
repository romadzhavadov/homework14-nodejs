import { BaseController } from './base.controller';
import { UsersService } from '../service/users.service';
class UsersController extends BaseController {
    constructor() {
        const usersRepository = new UsersService();
        super('users', usersRepository);
    }
}
export default UsersController;
//# sourceMappingURL=Users.controller.js.map