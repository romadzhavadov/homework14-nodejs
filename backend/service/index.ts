import { NewsService } from "./news.service"
import { UsersService } from "./users.service"

export {
    NewsService,
    UsersService
}