import { MigrationInterface, QueryRunner, TableColumn } from "typeorm";

export class Migrations1712327008785 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        // Додаємо поле "deleted" до таблиці "newpost" та "users"
        await queryRunner.addColumn("newspost", new TableColumn({
            name: "deleted",
            type: "boolean",
            default: false
        }));

        await queryRunner.addColumn("users", new TableColumn({
            name: "deleted",
            type: "boolean",
            default: false
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn("newspost", "deleted");
        await queryRunner.dropColumn("users", "deleted");
    }

}