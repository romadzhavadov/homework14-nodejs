import PropTypes from "prop-types";
import styles from './Item.module.scss';
import { Link } from 'react-router-dom';
import axios from "axios";
import { useNavigate } from 'react-router-dom';
import { useDispatch} from "react-redux";
import { fetchItems } from '../../redux/silces/itemsSlice';

const Item = ({ id, title, text, isPrivate, genre, author}) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  console.log(author)
  const handleDelete = async (e) => {
    e.preventDefault();
    try {
      if (id) {
        const data = await axios.delete(`http://localhost:8000/api/newsposts/${id}`);
        navigate('/home');
        console.log(data)
      } 
      await dispatch(fetchItems())

    } catch (error) {
      console.error('Error saving news post:', error);
    } 
  };

  return (
    <Link to={`/${id}`}>
      <div className={styles.root}>
        <div className={styles.contentWrapper}>
          <div className={styles.info}>
            <div className={styles.name}>{title}</div>
            <div className={styles.description}>{text}</div>
            {author && <div className={styles.description}>Author: {author.email}</div>}
            <div className={styles.genre}>Topic: {genre}</div>
          </div>
        </div>
        <div className={styles.btnRoot}>
          <button onClick={handleDelete}>Delete News</button>
        </div>
      </div>
    </Link>
  )
}

Item.propTypes = {
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  author: PropTypes.shape({
    name: PropTypes.string,
    age: PropTypes.number,
    email: PropTypes.string
  }),
  name: PropTypes.string,
  description: PropTypes.string,
};

Item.defaultProps = {
  id: '',
  name: '',
  description: '',
  author: null
}

export default Item;