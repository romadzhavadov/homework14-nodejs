import styles from './Nav.module.scss';
import { NavLink } from "react-router-dom";
import { useSelector } from 'react-redux';
import classNames from "classnames";

const Nav = () => {
  const token = useSelector(state => state.token );
  console.log(token)

    return (
        <nav className={styles.nav}>
            <ul>
              <li>
                <NavLink className={({ isActive }) => classNames(styles.link, { [styles.active]: isActive })} to={!!token ? '/home' : '/'} end>News feed</NavLink>
              </li>
            </ul>
        </nav>
    )
}

export default Nav;