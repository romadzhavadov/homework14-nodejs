import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  token: null,
}

const tokenSlice = createSlice({
  name: "token",
  initialState,

  reducers: {
    setToken(state, action) {
      state.token = action.payload;
      localStorage.setItem('TOKEN', state.token);
    },
    removeToken(state) {
      state.token = null;
      localStorage.removeItem('TOKEN');
    },
  }
});

export const { setToken, removeToken } = tokenSlice.actions;

export default tokenSlice.reducer;

